#!/usr/bin/env python3
# from cassandra import wsgi
# from django.conf import settings
import os
import json
from networkx.readwrite import json_graph
import torch
from snli.model import SNLIModel
from utils.vocab import Vocab
import networkx as nx
from lxml import etree
from nltk import tokenize
import dill
import numpy as np
import datetime

torch.set_grad_enabled(False)
vocab = Vocab.from_file('pretrained/snli_vocab.txt', add_pad=True, add_unk=True)
model = SNLIModel(num_classes=3, num_words=len(vocab), word_dim=300, hidden_dim=300,
                  clf_hidden_dim=1024, clf_num_layers=1, bidirectional=False, dropout_prob=0,
                  use_leaf_rnn=True, intra_attention=False, use_batchnorm=True)
model.load_state_dict(torch.load('pretrained/snli_300d.pkl', map_location='cuda'))
model.eval()
def prepare_input(sent_words):
    word_ids = torch.LongTensor([[vocab.word_to_id(w) for w in sent_words]])
    length = torch.LongTensor([len(sent_words)])
    word_ids_emb = model.word_embedding(word_ids)
    return word_ids_emb, length

# model.encoder.forward(*prepare_input(['a', 'b', 'c']), return_select_masks=True)[2]
from nltk.tokenize import word_tokenize

# def to_tree_str(g):
def make_graph(output, sentence):

# #     if len(g.graph['sentence']) in [0,1]: # Degenerate sentence
# #         for w in sentence:
# #             g.add_node(len(g),label=str(w))
# #         return
# #         return g, len(g)-1, [[],[]]
#     words = word_tokenize(g.graph['sentence'].lower())
#     if len(words) in [0,1]:
#         for w in words:
#             g.add_node(len(g),label=str(w))
#         return
#     enc_input, enc_length = prepare_input(words)
#     print(enc_input.shape)
#     output = model.encoder.forward(enc_input, enc_length, return_select_masks=True)
    length = len(sentence)
    g = nx.DiGraph()
    selections = output[2]
    vectors = [output[0].tolist(), output[1].tolist()]
    selections = [s[0].max(0)[1] for s in selections] + [0]
#     print(selections)
#     print(f"   Sentence: {sentence}")
    tokens = list(range(length))
    for w in sentence:
        g.add_node(len(g), label=str(w))
    for i in selections[:length-1]:
#         print(f"----selection:{i}")
        newidx = len(g)
        g.add_node(newidx, label=newidx)
        g.add_edge(newidx, tokens[i], idx=0)
#         if i + 1 < length:
        g.add_edge(newidx, tokens[i + 1], idx=1)
        del tokens[i:i+2]
        tokens.insert(i, newidx)
    assert len(tokens) == 1
    g.graph['root'] = len(g)-1
    g.graph['vectors'] = vectors
    return g
# hashpath = os.path.sep.join(['/var/lib/cassandra/files', 'graphs', 'by_hash', ''])
for path in [
        os.path.sep.join([settings.FILES_DIR, 'graphs']),
        os.path.sep.join([settings.FILES_DIR, 'graphs', 'by_hash'])
    ]:
    if not os.path.exists(path):
        os.mkdir(path)
hashpath = os.path.sep.join([settings.FILES_DIR, 'graphs', 'by_hash', ''])

if not os.path.exists(hashpath):
    os.mkdir(hashpath)
c = 0
sentences = []
print("Loading hashes...")
with open(settings.FILES_DIR + '/lookup_tables/hash_to_sentence_dict.dill','rb') as infile:
# with open('/var/lib/cassandra/files/lookup_tables/hash_to_sentence_dict.dill','rb') as infile:
    d = dill.load(infile)
print("Done")


batchsize = 10
sorted_by_size = sorted(list(d.keys()), key=lambda x: len(d[x]), reverse=True)

def do_batch(batch, maxlen):
    print(f"{datetime.datetime.now()}: Running batch of size {len(batch)}x{maxlen}")
    vectors = []
    lens = []
    for entry in batch:
        shit = model.word_embedding(
                torch.LongTensor([vocab.word_to_id(w) for w in entry] + [vocab.pad_id] * (maxlen - len(entry)))
            ).tolist()
        vectors.append(shit)
        lens.append(len(entry))
#     print("Done with prep, running")
    return model.encoder.forward(torch.Tensor(vectors),torch.LongTensor(lens), return_select_masks=True)
batch = []
ids = []
mlen = 0
while(True):
    try:
        # Will eventually throw an exception and terminate
        k = sorted_by_size.pop(0)
        graphpath = hashpath + str(k)
        if os.path.exists(graphpath):
            continue
        else:
            batch.append(d[k])
            ids.append(str(k))
            mlen = max(mlen,len(d[k]))
            if len(batch) == batchsize:
                v0, v1, selections = do_batch(batch, mlen)
                print(f"Batch len: {len(batch)}, id len: {len(ids)}")
                for i in range(len(batch)):
                    g = make_graph((
                                v0[i],
                                v1[i],
                                [elem[i] for elem in selections]
                            ),
                            batch[i]
                        )
    #                     shash = ids[i]
                    filepath = hashpath + ids[i]
    #                 print(f"Saving {filepath}")
                    with open(filepath,'w') as outfile:
                        json.dump(json_graph.cytoscape_data(g), outfile)
                mlen = 0
                batch = []
                ids = []
    except IndexError as e:
        raise e
        print("Done.")
        break

#         filepath = hashpath + file
#     #     print(filepath)
#         with open(filepath,'r') as infile:
#             g = json_graph.cytoscape_graph(json.load(infile))
#         c += 1
#         if ('parsed' in g.graph and g.graph['parsed'] == True) or len(g):
#             print(f"{filepath} already parsed")
#             continue
#         g.graph['parsed'] = True
#         with open(filepath,'w') as outfile:
#             json.dump(json_graph.cytoscape_data(g), outfile)





# for key in sorted_by_size:
#     graphpath = hashpath + str(key)
#     print(graphpath)
#     if os.path.exists(graphpath):
#         continue

# for k, v in dc.items():
#     print(k,v)







# l = 0
# batchsize = 50
# for k,v in d.items():
#     l = max(l,len(v))
#     c += 1
#     if c % 1000 == 0:
#         print(c)
#     if c == batchsize:
#         break
# print(f"Max length: {l}")
# # vectors = np.full((batchsize,l),fill_value=vocab.pad_id)
# # vectors = pytorch.LongTensor([l,batchsize,model.word_dim])
# lens = []
# vectors = []
# # vectors = torch.LongTensor(shape=[batchsize, l, model.word_dim])
# # print(f"vectors has shape {vectors.shape}")
# for e, (k, v) in enumerate(d.items()):
#     print(e)
#     if e == batchsize:
#         break
# #     vectors[e,0:len(v)] = [vocab.word_to_id(w) for w in v]
#     shit = model.word_embedding(
#             torch.LongTensor([vocab.word_to_id(w) for w in v] + [vocab.pad_id] * (l - len(v)))
#         ).tolist()
#     vectors.append(shit)
# #     vectors[e,:,:] = shit
# #     print(emb.size())
# #     )
#     lens.append(len(v))
# # print(vectors.size)s
# # print(np.array(lens).size)
# print(model.encoder.forward(torch.Tensor(vectors),torch.LongTensor(lens)))
