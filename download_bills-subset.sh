#!/bin/bash

tmpfile=`mktemp`
#zOMG hax
grep ^BILLS_DIR cassandra/settings.py > $tmpfile
. $tmpfile
echo Downloading bills to $BILLS_DIR ...
cd $BILLS_DIR

# Just grab the house joint resolutions for congress 113, session 1.
# This is a small slice of the full dataset, suitable for testing. Kinda.
dirs="hjres sres sjres"
congresses="113"
sessions="1"
for congress in $congresses
do
    for session in $sessions
    do
        for dir in $dirs
        do
        #https://www.govinfo.gov/bulkdata/BILLSTATUS/115/sres/BILLSTATUS-115-sres.zip
            url="https://www.govinfo.gov/bulkdata/BILLS/${congress}/${session}/${dir}/BILLS-${congress}-${session}-${dir}.zip"
            wget -c $url
        done
    done
done
rm $tmpfile
