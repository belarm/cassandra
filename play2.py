from cassandra import wsgi
# from django.conf import settings
from vote_prediction import utils
from vote_prediction.graph import Node, Edge
import datetime

# Round about 4.5 minutes with 3.3 million nodes, all sentences embedded (not quite worst-case, but close)

def bark(msg):
    print(f"{datetime.datetime.now()}: {msg}")


openquery = 'match(n) where not exists (n.h) return id(n) as id'
closedquery = 'match(n) where exists (n.h) return id(n) as id'
hydratequery = "match(n) where id(n) in $nodes return {id:id(n), h:n.h, c:n.c} as n"
relquery = 'match (s)-->(d) where id(s) in $open with s,d return id(s) as src,id(d) as dst'

def query_as_set(session, query, key='id', **params):
    res = set()
    for row in session.run(query, **params):
        if(len(res)) % 10000 == 0:
            print(len(res))
        res.add(row[key])
    return res
def query_as_set_of_tuples(session, query, keys, **params):
    res = set()
    for row in session.run(query, **params):
        # if(len(res)) % 10000 == 0:
            # print(len(res))
        res.add(tuple([row[k] for k in keys]))
    return res

def bootstrap():
    with utils.settings.GRAPH.session() as session:
        bark("Getting open set...")
        openset = query_as_set(session, openquery, 'id')
        # bark("Getting closed set...")
        # closed = query_as_set(session, closedquery, 'id')
        bark("Getting relationships...")
        edges = query_as_set_of_tuples(session, relquery,keys=['src', 'dst'], open=list(openset))
    bark("Calculating closed set...")
    closed = set()
    for edge in edges:
        if edge[1] not in openset:
            closed.add(edge[1])
    # fringe = openset.difference(closed)
    # Now, we need h & c for all candidates
    bark("Building id_to_node")
    id_to_node = {}
    for idx in openset.union(closed):
        id_to_node[idx] = Node(id=idx)
    bark("Hydrating closed set...")

    with utils.settings.GRAPH.session() as session:
        res = query_as_set(session, hydratequery, key='n', nodes=list(closed))
    bark("Got results, storing...")
    for row in res:
        id_to_node[row['id']].properties['h'] = row['h']
        id_to_node[row['id']].properties['c'] = row['c']
    bark("Done")
    return openset, closed, edges, id_to_node
