#!/bin/bash
dirs="sres sjres sconres s hres hr hjres hconres"
congresses="113 114 115"
# sessions="1 2"
for congress in $congresses
do
#     for session in $sessions
#     do
        for dir in $dirs
        do
        #https://www.govinfo.gov/bulkdata/BILLSTATUS/115/sres/BILLSTATUS-115-sres.zip
            url="https://www.govinfo.gov/bulkdata/BILLSTATUS/${congress}/${dir}/BILLSTATUS-${congress}-${dir}.zip"
            wget -c $url
        done
#     done
done
