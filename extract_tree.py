# Functional code from https://github.com/jihunchoi/unsupervised-treelstm/blob/master/extract_trees.ipynb
### Example code for extracting trees from a trained model file.
import torch
from snli.model import SNLIModel
from utils.vocab import Vocab
torch.set_grad_enabled(False)
vocab = Vocab.from_file('pretrained/snli_vocab.txt', add_pad=True, add_unk=True)
model = SNLIModel(num_classes=3, num_words=len(vocab), word_dim=300, hidden_dim=300,
                  clf_hidden_dim=1024, clf_num_layers=1, bidirectional=False, dropout_prob=0,
                  use_leaf_rnn=True, intra_attention=False, use_batchnorm=True)
model.load_state_dict(torch.load('pretrained/snli_300d.pkl', map_location='cpu'))
print(model.eval())
def prepare_input(sent_words):
    word_ids = torch.LongTensor([[vocab.word_to_id(w) for w in sent_words]])
    length = torch.LongTensor([len(sent_words)])
    word_ids_emb = model.word_embedding(word_ids)
    return word_ids_emb, length
model.encoder.forward(*prepare_input(['a', 'b', 'c']), return_select_masks=True)[2]
from nltk.tokenize import word_tokenize
def to_tree_str(sentence):
    words = word_tokenize(sentence.lower())
    enc_input, enc_length = prepare_input(words)
    selections = model.encoder.forward(enc_input, enc_length, return_select_masks=True)[2]
    selections = [s[0].max(0)[1] for s in selections] + [0]
    tokens = words.copy()
    for i in selections:
        composed = f'( {tokens[i]} {tokens[i+1]} )'
        del tokens[i:i+2]
        tokens.insert(i, composed)
    assert len(tokens) == 1
    return tokens[0]
print(to_tree_str('Two kids are running down a highway.'))
