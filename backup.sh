#!/bin/bash
filename=~/.backups/cassandra-$(date +%Y-%m-%d-%H-%M-%S).tar.gz
export IFS=$'\n\t'
echo "$filename"
cd ..
tar -c cassandra | gzip -c > $filename
cd cassandra
