from django.db import models
from django.db import transaction
import dill
from django.conf import settings
import os
from networkx.readwrite import json_graph
import networkx as nx
import json
from nltk import tokenize
from nltk.tokenize.treebank import TreebankWordTokenizer
from lxml import etree
from zlib import crc32, adler32
import io
import mimetypes
from django.contrib.postgres.fields import ArrayField
class NodeInfo(models.Model):
    id = models.IntegerField(primary_key=True)
    h = ArrayField(models.FloatField(null=True), null=True)
    c = ArrayField(models.FloatField(null=True), null=True)
    text = ArrayField(models.TextField(null=True), null=True) # Maybe?

def file_path(uid):
    return os.path.sep.join([settings.FILES_DIR, str(uid)])

def open_file(uid, mode='rb'):
    return open(file_path(uid), mode)

def graph_path(node, id=0):
    return os.path.sep.join([settings.FILES_DIR, 'graphs', f"{node['name']}-{str(id).zfill(5)}.json"])

def graph_sentences(node):
    for i in range(node['sentences']):
        with open(graph_path(node, i),'r') as infile:
             g = json_graph.cytoscape.cytoscape_graph(json.load(infile))
        yield ' '.join([g.node[a]['label'] for a in nx.dfs_preorder_nodes(g) if g.out_degree(a) == 0])
def sentence_graphs(node):
    for i in range(node['sentences']):
        with open(graph_path(node, i),'r') as infile:
             yield json_graph.cytoscape.cytoscape_graph(json.load(infile))
#         yield ' '.join([g.node[a]['label'] for a in nx.dfs_preorder_nodes(g) if g.out_degree(a) == 0])
def store_uploaded_file(uid, f):
    '''Given a uid and a django file handle, store the file with a name based on the uid'''
#     with open(os.path.sep.join([settings.FILES_DIR, str(uid)]), 'wb+') as destination:
    with open_file(uid, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def store_file(f, name=None):
    '''Given a file path or object, create a node and store the file with a name reflecting its UID
    If passing an open file, the name argument is required'''
    if isinstance(f, io.IOBase):
        if name is None:
            raise ValueError('name argument is required when saving open files.')
    else:
        # Then it must be a path
        name=f.split(os.path.sep)[-1]
        f = open(f, 'rb')
    mimetype, encoding = mimetypes.guess_type(name)
    res = settings.GRAPH.run(f"CREATE (n:File {{name: '{name}', mimetype: '{mimetype}'}}) RETURN n").data()
    uid = res[0]['n'].identity
    with open_file(uid, 'wb+') as destination:
        destination.write(f.read())
    return uid

vector_to_sentence_dict = {}
word_tokenizer = TreebankWordTokenizer()

def hash64(data):
    '''Returns a very fast 64-bit hash of data:bytes.
    Note that this is not even REMOTELY cryptographically secure!'''
    return adler32(data) << 31 ^ crc32(data)

def sentences_from_xml(et, lower=True):
#     if not isinstance(et, etree.ElementTree):
#         if isinstance(et, etree.Element):
#             root = et
#         else: # Try to treat it as a node

#     else:
    root = et.getroot()
    text = []
    for child in root.iter():
        if child.text:
            if lower:
                sentences = tokenize.sent_tokenize(child.text.lower())
            else:
                sentences = tokenize.sent_tokenize(child.text)
            for sent in sentences:
                text.append(word_tokenizer.tokenize(sent))
    return text

def xml_from_node(node):
    return etree.parse(file_path(node.identity))

def sentences_from_node(node, return_xml=False):
    if not return_xml:
        return sentences_from_xml(xml_from_node(node))
    else:
        xml = xml_from_node(node)
        return sentences_from_xml(xml), xml

def hashed_sentences_from_node(node, return_xml=False):
    xml = xml_from_node(node)
    bare_sentences = sentences_from_xml(xml)
    sentence_pairs = []
    for sent in bare_sentences:
        sentence_pairs.append((hash64(str.encode(' '.join(sent))), sent))
    if not return_xml:
        return sentence_pairs
    else:
        return sentence_pairs, xml

def hashed_sentences_from_uid(uid, return_xml=False):
    xml = etree.parse(file_path(uid))
    bare_sentences = sentences_from_xml(xml)
    sentence_pairs = []
    for sent in bare_sentences:
        sentence_pairs.append((hash64(str.encode(' '.join(sent))), sent))
    if not return_xml:
        return sentence_pairs
    else:
        return sentence_pairs, xml


def hashed_sentences_from_element(element, return_sentences=True):
    for sent in tokenize.sent_tokenize(element.text.lower()):
        # print("------hashing:")
        # print(sent)
        if return_sentences:
            yield hash64(str.encode(' '.join(word_tokenizer.tokenize(sent)))), sent
        else:
            yield hash64(str.encode(' '.join(word_tokenizer.tokenize(sent))))
def graph_from_db():
    g = nx.DiGraph()
    # res = settings.GRAPH.run("MATCH p=((s:File {mimetype:'application/xml'})-[*0..]->(x)) return x,startNode(last(relationships(p))) as parent, p")
    res = settings.GRAPH.run("MATCH p=((s:File {mimetype:'application/xml'})-[*0..]->(x)) return x,startNode(last(relationships(p))) as parent, p")
    d = res.data()
    for r in d:
        if r['x'].identity not in g:
            g.add_node(r['x'].identity, **r['x'])
            g.node[r['x'].identity]['_labels'] = r['x'].labels
        if r['parent'] is not None:
            if r['parent'].identity not in g:
                g.add_node(r['parent'].identity, **r['parent'])
                g.node[r['parent'].identity]['_labels'] = r['parent'].labels

            # print(r['parent'].identity, r['x'].identity)
            g.add_edge(r['parent'].identity, r['x'].identity)
    return g
