# #!/usr/bin/env python3
# import neo4j
# from django.conf import settings
# import networkx as nx
# from networkx.drawing.nx_pydot import write_dot
# from .models import NodeInfo
# from pprint import pprint
# import itertools
# create_relationship_stmt = """
#     UNWIND $relationships as rel
#     MATCH (s) WHERE ID(s) = rel.tail.id WITH s, rel
#     MATCH (d) WHERE ID(d) = rel.head.id WITH s, d, rel
#     CREATE (s)-[r:{label}]->(d) with r, rel
#     SET r = rel.properties
#     RETURN r"""
# create_node_stmt = """
#     UNWIND $nodes as node
#     CREATE (n:{label}) WITH n, node
#     set n = node.properties
#     RETURN n"""
#
#
# class Element(object):
#     '''Base class for Nodes, Edges, and Graphs'''
#     def __init__(self, labels=None, properties=None):
#         if labels is None:
#             self.labels = set()
#         else:
#             self.labels = labels
#         if properties is None:
#             self.properties = dict()
#         else:
#             self.properties = properties
#
#     def jsonify(self):
#         ret = {}
#         for k, v in self.__dict__.items():
#             if k == 'labels':
#                 ret[k] = ':'.join(v)
#             else:
#                 # print(type(v), isinstance(v, Element))
#                 if isinstance(v, Element):
#                     # print("Stuff....")
#                     v = v.jsonify()
#                 if isinstance(v, neo4j.v1.types.graph.Node):
#                     v = Node.from_neo4j(v).jsonify()
#                 ret[k] = v
#         return ret
#
#     def pprint(self):
#         pprint(self.jsonify())
#
#
#
# # Nodes should be singletons. We'll need a class-level registry of them.
# # That won't actually work because of the use case where we build nodes client-side
# # and then create them in the DB
# class Node(Element):
#     def __init__(self, labels=None, properties=None, id=None):
#         super().__init__(labels, properties)
#         self.id = id
#
#     @classmethod
#     def from_neo4j(cls, node):
#         # print(dict(node))
#         return Node(
#             labels=node.labels,
#             properties=dict(node),
#             id=node.id
#         )
#
#     def __str__(self):
#         return f"Node({self.id})"
#
#     def __repr__(self):
#         return str(self)
#
#     def to_h_and_c_update(self):
#         return {
#             'id': self.id,
#             'h' : self.properties['h'],
#             'c' : self.properties['c'],
#         }
#
# class Edge(Element):
#     def __init__(self, tail=None, head=None, labels=None, properties=None):
#         super().__init__(labels, properties)
#         self.head = head
#         self.tail = tail
#
#     def __str__(self):
#         label=':'.join(self.labels)
#         return f'({self.tail.id})-[:{label}]->({self.head.id})'
#
# def setsort(x):
#     return tuple(x.labels)
#
# # And this is how we make UNWIND queries from a list of Nodes
# def parentsof(children):
#     '''Given a list of py2neo Nodes, return a cursor of their parents
#     as [{'n':Node}]'''
#     return settings.GRAPH.run(
#         get_parent_stmt,
#         children=[
#             {'uid':x.identity} for x in children
#         ]
#     )
#
# def childrenof(parents):
#     '''Given a list of py2neo Nodes, return a cursor of their children
#     as [{'n':Node}]'''
#     with settings.GRAPH.session() as session:
#         return session.run(
#             get_children_stmt,
#             parents=[
#                 {'uid':x.id} for x in parents
#             ]
#         )
#
# def parentsof_g(children_generator=None):
#     '''Given a py2neo cursor returning nodes as 'n',
#     return a cursor of their parents as [{'n':Node}]'''
#     # This permits parentsof_g(leaves()), for example
#     if children_generator is None:
#         # parentsof(None) = leaves
#         return leaves()
#     return settings.GRAPH.run(
#         get_parent_stmt,
#         children=[
#             {'uid':x['n'].identity} for x in children_generator
#         ]
#     )
#
# def childrenof_g(parents_generator=None):
#     '''Given a py2neo cursor returning nodes as 'n',
#     return a cursor of their children as [{'n':Node}]'''
#     # This permits childrenof_g(roots()), for example
#     if parents_generator is None:
#         # childrenof(None) = roots
#         return roots()
#     return settings.GRAPH.run(
#         get_children_stmt,
#         children=[
#             {'uid':x['n'].identity} for x in parents_generator
#         ]
#     )
#
# def leaves(ordered=False):
#     '''Return a generator of all leaf (0 out-degree) nodes'''
#     if ordered:
#         with settings.GRAPH.session() as session:
#             return session.run(ordered_leaf_node_stmt)
#     else:
#         with settings.GRAPH.session() as session:
#             return session.run(leaf_node_stmt)
# def roots():
#     '''Return a generator of all root (0 in-degree) nodes'''
#     with settings.GRAPH.session() as session:
#         return session.run(root_node_stmt)
#
# def push_nodes(nodes):
#     '''Given a list of nodes, commits each to the database
#     and returns a list of the updated node objects.'''
#     # neo4j
#     pass
# get_parent_stmt = """
#     UNWIND $children as child
#     MATCH (n)<--(parent) WHERE ID(n) = child.uid
#     RETURN n"""
# get_children_stmt = """
#     UNWIND $parents as parent
#     MATCH (n)-->(child) WHERE ID(n) = parent.uid
#     RETURN n"""
#
# leaf_node_stmt = """
#     match (n)
#     where not (n)-->()
#     return distinct n"""
#
#
# # Only get leaves we haven't hit.
# ordered_leaf_node_stmt = """
#     match (n)
#     where not (n)-->()
#     AND NOT EXISTS(n.h)
#     with distinct n
#     return id(n) as id
#     order by size(n.text)"""
# # ordered_leaf_node_stmt = """
# #     match (n)
# #     where not (n)-->()
# #     return distinct n
# #     order by size(n.text)"""
# root_node_stmt = """
#     match (n)
#     where not ()-->(n)
#     return distinct n"""
