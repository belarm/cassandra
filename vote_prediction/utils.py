from cassandra import wsgi
from django.db import transaction

import os
import io
from collections import defaultdict, namedtuple
from django.conf import settings
from .models import NodeInfo
import mimetypes
import zipfile
# import py2neo
import torch
from lxml import etree
from nltk import tokenize
from nltk.tokenize.treebank import TreebankWordTokenizer
from zlib import crc32, adler32
from snli.model import SNLIModel
from utils.vocab import Vocab
# from .graph import Node, Edge, push_updates
import neo4j
import json
import datetime
import redis
import networkx as nx
import functools
import itertools
class lookup_tables:
    '''Simple container for some handy data objects.
    Not intended to be instantiated'''
    kdtree = None
    hash_to_nodeid = None
    hash_to_sentence = None

def bark(msg):
    print(f"{datetime.datetime.now()}: {msg}")

#Load the model. Nice and simple /s
# Well, it works...
torch.set_grad_enabled(False)
vocab = Vocab.from_file('pretrained/snli_vocab.txt', add_pad=True, add_unk=True)
model = SNLIModel(num_classes=3, num_words=len(vocab), word_dim=300, hidden_dim=300,
                  clf_hidden_dim=1024, clf_num_layers=1, bidirectional=False, dropout_prob=0,
                  use_leaf_rnn=True, intra_attention=False, use_batchnorm=True)
model.load_state_dict(torch.load('pretrained/snli_300d.pkl', map_location='cpu'))
null_vector = model.word_embedding(torch.as_tensor([[vocab.pad_id]]))
null_state = model.encoder.forward(null_vector,torch.as_tensor([1]))
null_h = null_state[0][0]
null_c = null_state[1][0]
sess = settings.GRAPH.session()
sess.run("CREATE INDEX ON :File(mimetype)")
sess.run("CREATE INDEX ON :Sentence(hash)")
# settings.GRAPH.run("CREATE CONSTRAINT ON (s:Sentence) ASSERT s.hash IS UNIQUE")

word_tokenizer = TreebankWordTokenizer()

# utilty class to represent pending relationships
RelationshipData = namedtuple('RelationshipData', 'srcid dstid type attr')

# Cypher queries
find_xml_query = """MATCH (n:File {mimetype:'application/xml'}) WHERE n.processed = False RETURN n"""
# find_xml_query = """MATCH (n:File {mimetype:'application/xml'}) WHERE n.processed = False RETURN n limit 1"""
find_senteces_query = """MATCH (n:SENTENCE) RETURN n limit 1000"""
file_node_stmt = "match(n:File) return n"


# Need to use python formatting here because neo4j doesn't support setting
# relationship attributes at creation time.
create_relationship_stmt = """
    UNWIND $relationships as rel
    MATCH (s) WHERE ID(s) = rel.tail.id WITH s, rel
    MATCH (d) WHERE ID(d) = rel.head.id WITH s, d, rel
    CREATE (s)-[r:{label}]->(d) with r, rel
    SET r = rel.properties
    RETURN r"""

create_nodes_stmt = """
    UNWIND $nodes as node
    CREATE (n:{label}
    {{done:False}})
    RETURN n"""
#     RETURN n"""
create_sentences_stmt = """
    UNWIND $sentences as sent
    MERGE (n:Sentence
    {
        hash:sent.hash,
        done:False
    }
    )
    RETURN n"""



def file_path(uid):
    '''Returns a unique path associated with the node having ID=uid'''
    return os.path.sep.join([settings.FILES_DIR, str(uid)])

def open_file(uid, mode='rb'):
    '''Opens the file associated with a node (given by ID)'''
    return open(file_path(uid), mode)

def find_all_files(path):
    '''A generator that recursively iterates a directory,
    yielding the relative paths of any files found'''
    for dirpath, dirs, files in os.walk(path):
        for f in files:
            yield os.path.sep.join([dirpath, f])

def store_file(f, name=None, mimetype=None):
    '''Given a file path (as a string) or file-like object, create a node and store the file with a
    name reflecting its UID. If passing an open file, the name argument is required'''
    if isinstance(f, io.IOBase):
        if name is None:
            raise ValueError('name argument is required when saving open files.')
    else:
        # Then it must be a path
        name=f.split(os.path.sep)[-1]
        f = open(f, 'rb')
    if mimetype is None:
        mimetype, encoding = mimetypes.guess_type(name)
    with settings.GRAPH.session() as session:
        res = session.run(f"CREATE (n:File {{name: '{name}', mimetype: '{mimetype}', processed: False, done: False}}) RETURN n").data()
    uid = res[0]['n'].id
    ni = NodeInfo.objects.create(id=uid)
    ni.save()
    with open_file(uid, 'wb+') as destination:
        destination.write(f.read())
    return res[0]['n']

def upload_directory(path=settings.BILLS_DIR):
    with settings.GRAPH.session() as session:
        existingfiles = set([r['n']['name'] for r in session.run(file_node_stmt)])
    '''Given a path, recursively stores all files therein in the graph database'''
    for f in find_all_files(path):
        name=f.split(os.path.sep)[-1]
        if name not in existingfiles:
            print(f"Storing {f}")
            store_file(f)
        else:
            print(f"Duplicate filename {f}, skipping")

def unzip_zipfiles():
    '''Unzips any nodes representing zipfiles'''
#     session = settings.GRAPH.session()
    with settings.GRAPH.session() as session:
        for _node in session.run("match(n:File {mimetype:'application/zip'}) WHERE n.processed = False return n"):
            node = _node['n']
            zipfilepath = file_path(node.id)
            zf = zipfile.ZipFile(
                zipfilepath
            )
            print("Unzipping {}".format(node['name']))
            for zinfo in zf.infolist():
                mimetype, encoding = mimetypes.guess_type(zinfo.filename)
                newnode = session.run("CREATE (n:File {{name: '{}', mimetype: '{}', processed: False}}) RETURN n".format(
                    zinfo.filename,
                    mimetype)).data()[0]['n']
                ni = NodeInfo.objects.create(id=newnode.id)
                with zf.open(zinfo) as infile:
                    with open_file(newnode.id, 'wb') as outfile:
                        outfile.write(infile.read())
    #             rel = Edge(head=node, tail=newnode, labels={'CONTAINS'})
                res = session.run(
                    create_relationship_stmt.format(label='CONTAINS'),
                    relationships=[{
                        'tail':{'id':node.id},
                        'head':{'id':newnode.id},
                        'properties':{}
                    }]
                )
            session.run(f'match(n) where ID(n)={node.id} set n.processed=True')

def hash64(data):
    '''Returns a very fast unsigned 63-bit hash of data:bytes.
    Note that this is not even REMOTELY cryptographically secure!'''
    return adler32(data) << 31 ^ crc32(data)


def hash_combine(lhs, rhs):
    '''Combine two hashes, respecting order'''
    # same logic as boost++
    return lhs ^ (rhs + 0x9e3779b9 + (lhs << 6) + (lhs >> 2))


################################################################################
# This entire section needs to be taken out and burned
################################################################################

def hashed_sentences_from_element(element, return_sentences=True):
    for _sent in tokenize.sent_tokenize(element.text.lower()):
        # print("------hashing:")
        # print(sent)
        to_parse = _sent.split(';')
        for sent in to_parse:
            tokenized = word_tokenizer.tokenize(sent)
            if return_sentences:
                yield hash64(str.encode(' '.join(tokenized))), tokenized
            else:
                yield hash64(str.encode(' '.join(tokenized)))

def add_sentence_nodes(graph, element):
    '''Gets hashed sentences from the given element's text and adds them as children
    of the element in the given graph'''
    if element.text:
        for hash, sentence in hashed_sentences_from_element(element):
            graph.add_node(hash, labels={'SENTENCE'}, hash=hash, text=sentence)
            graph.add_edge(element, hash, labels={'TEXT'})

def xml_from_node(node):
    '''Returns an XML tree from a given (py2neo) Node object'''
    return etree.parse(file_path(node.id))

def parse_xml_files():
    '''Parses any xml files in the graph, creating child
    nodes and relationships to represent their structure'''
    c = 0
    with settings.GRAPH.session() as session:
        for _node in session.run(find_xml_query):
            n = _node['n']
    #         print(n)
            try:
    #             node = Node.from_neo4j(n)
                xmltree = xml_from_node(n)
                g = nx.DiGraph()
                for elem in xmltree.iter():
                    g.add_node(elem, labels={'XMLElement'})
                    g.add_edge(elem.getparent(), elem, labels={'CONTAINS'})
                    add_sentence_nodes(g, elem)
                g.remove_node(None) # Remove extraneous 'root' node
                c += 1
                push_updates(g, n)
            except Exception as e:
                if isinstance(e, etree.XMLSyntaxError):
                    print("Malformed XML: {}".format(n))
                else:
                    raise e
            session.run('match(n) where id(n)=$id set n.processed = True', id=n.id)
            if c % 100 == 0:
                bark(f"Finished with {c} bills")

def push_updates(graph, rootnode):
    def donodes(graph):
        keyfunc = lambda x: graph.node[x].get('labels', None)
        sorted_nodes = sorted(graph, key=keyfunc)
        for labels, generator in itertools.groupby(sorted_nodes, key=keyfunc):
            if labels == {'SENTENCE'}: # Special case for sentences, MERGE instead of CREATE:
                texts = []
                with settings.GRAPH.session() as session:
                    # Consume the generator and store results locally.
                    to_make = [_ for _ in generator]
                    res = session.run(
                        create_sentences_stmt,
                        sentences=[{'hash':graph.node[n]['hash']} for n in to_make]
                    )
                    # Save the created ids to our relational database
                    with transaction.atomic():
                        for node, r in zip(to_make, res):
                            idx = r['n'].id
                            graph.node[node]['id'] = idx
    #                         ni = NodeInfo(id=idx)
                            ni, created = NodeInfo.objects.get_or_create(id=idx)
                            if created:
                                ni.text = graph.node[node]['text']
                                ni.save()
            else:
                with settings.GRAPH.session() as session:
                    # Consume the generator and store results locally.
                    to_make = [_ for _ in generator]
                    label = ':'.join(graph.node[to_make[0]]['labels'])
                    res = session.run(
                        create_nodes_stmt.format(label=label),
                        nodes=list(range(len(to_make)))
                    )
                    # Save the created ids to our relational database
                    with transaction.atomic():
                        for node, r in zip(to_make, res):
                            idx = r['n'].id
                            graph.node[node]['id'] = idx
                            ni = NodeInfo(id=idx)
                            ni.save()

#                         if created:
#                             ni.text = graph.node[node]['text']
#                             ni.save()
    # def donodes():
    #     to_submit = []
    #     lastlabels = set()
    #     for node in sorted_nodes:
    #         if lastlabels != node.labels:
    #             if len(to_submit) > 0:
    #                 with settings.GRAPH.session() as session:
    #                     res = session.run(
    #                         create_node_stmt.format(label=':'.join(lastlabels)),
    #                         nodes=[_.jsonify() for _ in to_submit]
    #                     )
    #                 with database.transaction.atomic():
    #                     for e, r in enumerate(res):
    #                         _n = r['n']
    #                         to_submit[e].id = _n.id
    #                         ni = NodeInfo.objects.create(id=_n.id)
    #                         ni.save()
    #             to_submit = []
    #             lastlabels = node.labels
    #         to_submit.append(node)
    #     if len(to_submit) > 0:
    #         with settings.GRAPH.session() as session:
    #             res = session.run(
    #                 create_node_stmt.format(label=':'.join(lastlabels)),
    #                 nodes=[_.jsonify() for _ in to_submit]
    #             )
    #         with database.transaction.atomic():
    #             for e, r in enumerate(res):
    #                 _n = r['n']
    #                 to_submit[e].id = _n.id
    #                 ni = NodeInfo.objects.create(id=_n.id)
    #                 ni.save()
    def doedges(graph):
        keyfunc = lambda x: x[2].get('labels', None)
        sorted_nodes = sorted(graph.edges(data=True), key=keyfunc)
        for labels, generator in itertools.groupby(sorted_nodes, key=keyfunc):
            with settings.GRAPH.session() as session:
                session.run(
                    create_relationship_stmt.format(label=':'.join(labels)),
                    relationships=[
                        {
                            'tail': {'id':graph.node[u]['id']},
                            'head': {'id':graph.node[v]['id']},
                            'properties':{}
                        } for (u, v, p) in generator
                    ],
                )


    # def doedges():
    #     to_submit = []
    #     lastlabels = {}
    #     for edge in sorted_edges:
    #         if lastlabels != edge.labels:
    #             if len(to_submit) > 0:
    #                 with settings.GRAPH.session() as session:
    #                     res = session.run(create_relationship_stmt.format(label=':'.join(lastlabels)), relationships=to_submit)
    #             to_submit = []
    #             lastlabels = edge.labels
    #         to_submit.append(edge.jsonify())
    #     if len(to_submit) > 0:
    #         with settings.GRAPH.session() as session:
    #             res = session.run(create_relationship_stmt.format(label=':'.join(lastlabels)), relationships=to_submit)

    donodes(graph)
    graphroot = [graph.node[n] for n in graph if graph.in_degree(n) == 0][0]
    with settings.GRAPH.session() as session:
        session.run(
            create_relationship_stmt.format(label='CONTAINS'),
            relationships=[{
                'tail':{'id':rootnode.id},
                'head':{'id':graphroot['id']},
                'properties':{}
            }]
        )
    doedges(graph)
