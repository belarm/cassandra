from django.apps import AppConfig


class VotePredictionConfig(AppConfig):
    name = 'vote_prediction'
