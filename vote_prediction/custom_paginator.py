# from django.core import paginator
from django.core.paginator import Paginator, InvalidPage
from django.conf import settings
from django.utils.functional import cached_property
# from django.utils.translation import gettext_lazy as _
class CustomPaginator(Paginator):

    def __init__(
            self,
            label,
            per_page,
            filters={},
            orphans=0,
            allow_empty_first_page=True
        ):
        self.label = label
        self.filters = filters
#         self._check_object_list_is_ordered()
        self.per_page = int(per_page)
        self.orphans = int(orphans)
        self.allow_empty_first_page = allow_empty_first_page

    def page(self, number):
        """Return a Page object for the given 1-based page number."""
        number = self.validate_number(number)
        bottom = (number - 1) * self.per_page
        object_list = [
            res['n'] for res in settings.GRAPH.run(
                self.base_query + f"return n skip {bottom} limit {self.per_page}"
            ).data()
        ]
        for o in object_list:
            o['identity'] = o.identity
            o['labels'] = o.labels
        return self._get_page(object_list, number, self)

    @cached_property
    def base_query(self):
        query = f"match (n:{self.label} {{"
        isfirst=True
        for k,v in self.filters.items():
            if not isfirst:
                query += ','
            query += f"{k}:"
            if isinstance(v,str):
                query += f"'{v}'"
            else:
                query += f"{v}"
            isfirst = False
        query += "}) "
        return query

    @cached_property
    def count(self):
        """Return the total number of objects, across all pages."""
        query = self.base_query + "return count(n) as count"
        return settings.GRAPH.run(query).data()[0]['count']

    def _check_object_list_is_ordered(self):
        # Our query is intrinsically ordered
        pass
