from . import utils
from .models import NodeInfo
import datetime
import torch
# import py2neo
# from .graph import Node, Edge
from collections import defaultdict
import json
import pickle
import numpy as np
from django.conf import settings
allnodesquery = 'match(n) return id(n) as id'

from .utils import model, vocab, null_c, null_h, null_vector

class BinnedHeap(object):
    '''Holds sets of items grouped by the specified key function,
    setting .isready to True when one of the bins returns true when passed
    (as a set) to the threshold function.'''

    def __init__(self, key, threshold, trigger, postcall=lambda x: x):
        self.key = key
        self.threshold = threshold
        self.trigger = trigger
        self.postcall = postcall
        self._data = {}
        self.isready = False
        self.readybin = None

    def add(self, item, *args, **kwargs):
        '''Adds an item, determining its bin by calling key(item,*args,**kwargs)
        returns True if the target bin is ready.'''
        key = self.key(item, *args, **kwargs)
        if key not in self._data:
            self._data[key] = set()
        self._data[key].add(item)
        if self.threshold(self._data[key]):
            self.isready = True
            self.readybin = key
        return self.isready

    def runtrigger(self, targetbin=None, return_args=True):
        '''Runs self.trigger on the ready bin'''
        if targetbin is None:
            targetbin = self.readybin
        to_pass = list(self._data[targetbin])
        self._data[targetbin] = set()
        self.isready = False
        self.readybin = None
        if return_args:
            # print("returning args")
            return self.postcall((to_pass, self.trigger(to_pass)))
        else:
            # print("not returning args")
            return self.postcall(self.trigger(to_pass))

    def finish(self, return_args=True):
        '''Runs self.runtrigger() on all bins, returning results as a list'''
        res = []
        for k in sorted(self._data.keys()):
            if len(self._data[k]):
                res.append(self.runtrigger(targetbin=k, return_args=return_args))
        return res

leaf_node_stmt = """
    match (n)
    where n.done = False
    and not (n)-->()
    return id(n) as id"""
mark_nodes_done_stmt = """
    UNWIND $ids as id
    MATCH (n) WHERE ID(n) = id with n
    SET n.done = True
    RETURN n"""
frontier_query = """
    match (n) where n.done = false with n
    optional match (n)-->(e) with n, count(e) as childnum
    optional match (n)-->(d)
    where d.done = false with n, count(d) as num, childnum
    where num = 0
    return id(n) as id, childnum order by childnum
"""

frontier_query = """
MATCH
	(n)-->(e), (n)-->(d)
WITH
	n, e, d, count(e) as ce, count(d) as cd
WHERE
	n.done = False
    AND d.done = True
	AND ce = cd
RETURN
	id(n) as n, collect(distinct id(d)) as es
LIMIT 1000"""
def do_leaf_batch(batch):
    vectors = []
    lens = []
    for entry in batch:
        if entry.text is not None and entry.text != []:
            # print(entry.text)
            word_ids = torch.LongTensor(
                [vocab.word_to_id(w) for w in entry.text]
            )
            embedding = model.word_embedding(word_ids)
        else:
            embedding = null_vector[0]
            # print(embedding.size())
        vectors.append(embedding)
        lens.append(int(embedding.size(0)))
    vectors = torch.stack(vectors)
    lens = torch.LongTensor(lens)
    # print(f"Vector size: {vectors.size()}")
    print(f"{datetime.datetime.now()}: Running batch of size {len(batch)}x{lens[0]}")
    return model.encoder.forward(vectors,lens)

def process_leaf_results(res):
    ids = []
    for n, h, c in zip(res[0], res[1][0], res[1][1]):
        ids.append(n.id)
        n.h = h.tolist()
        n.c = c.tolist()
        n.save()
    with settings.GRAPH.session() as session:
        session.run(mark_nodes_done_stmt, ids=ids)

def embed_leaves():
    '''Calculates an embedding for all leaves in the graph'''
    binfunc = lambda x: len(x.text)
    thresholdfunc = lambda x: len(x) >= 100
    # triggerfunc = lambda x: print(x)
    triggerfunc = do_leaf_batch
    leafbin = BinnedHeap(
        key=binfunc,
        threshold=thresholdfunc,
        trigger=triggerfunc,
        postcall=process_leaf_results
    )

    with settings.GRAPH.session() as session:
        leaf_ids = session.run(leaf_node_stmt).data()
        for leaf in leaf_ids:
            node_info = NodeInfo.objects.get(id=leaf['id'])
            # if node_info.h is not None:
            #     continue
            if node_info.text is None:
                node_info.text = []
            if leafbin.add(node_info):
                leafbin.runtrigger()
        leafbin.finish()

def do_frontier_batch(batch):
    hs = torch.stack([b[1] for b in batch])
    cs = torch.stack([b[2] for b in batch])
    print(f"{datetime.datetime.now()}: Running batch of size {len(batch)}x{hs[0].size(0)}")

    return model.encoder.forward_with_branches(hs, cs)
def process_frontier_results(res):
    ids = []
    for n, h, c in zip(res[0], res[1][0], res[1][1]):
        # print(f"Saving node {n}")
        ids.append(n[0].id)
        n[0].h = h.tolist()
        n[0].c = c.tolist()
        n[0].save()
    with settings.GRAPH.session() as session:
        session.run(mark_nodes_done_stmt, ids=ids)

def embed_frontier():
    '''Calculates an embedding for all nodes in the frontier of the graph -
    those which are open, but have only closed children'''
    binfunc = lambda x: x[1].size(0)
    thresholdfunc = lambda x: len(x) >= 100
    triggerfunc = do_frontier_batch
    postcall = process_frontier_results
    # triggerfunc = do_batch
    frontierbin = BinnedHeap(key=binfunc, threshold=thresholdfunc, trigger=triggerfunc, postcall=process_frontier_results)
    # res = []
    c = 0
    with settings.GRAPH.session() as session:
        for r in session.run(frontier_query):
            c += 1
            # print(r['n'],r['es'])
            ni = NodeInfo.objects.get(id=r['n'])
            hs, cs = [], []
            for e in r['es']:
                einfo = NodeInfo.objects.get(id=e)
                hs.append(einfo.h)
                cs.append(einfo.c)
            hs = torch.Tensor(hs)
            cs = torch.Tensor(cs)
            if frontierbin.add((ni, hs, cs)):
                frontierbin.runtrigger()
        # Finish up
        for k in sorted(frontierbin._data.keys()):
            if len(frontierbin._data[k]):
                frontierbin.runtrigger(targetbin=k)
    if c == 0:
        print(f"{datetime.datetime.now()}: Processed no nodes, done.")
        return False
    else:
        print(f"{datetime.datetime.now()}: Processed {c} nodes.")
        return True


print(f"{datetime.datetime.now()}: doing leaves")

embed_leaves()
print(f"{datetime.datetime.now()}: Done with leaves, getting frontier")

while(True):
    if embed_frontier():
        continue
    else:
        break
