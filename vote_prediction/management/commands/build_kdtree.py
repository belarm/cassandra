from django.core.management.base import BaseCommand, CommandError
# from vote_prediction.models import vector_to_sentence_dict
from django.conf import settings
from sklearn.neighbors import KDTree
from vote_prediction.models import NodeInfo
import os
import numpy as np
import dill

# r = settings.REDIS

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        kdtree_path = os.path.sep.join([settings.LUTABLES_DIR, 'kdtree.dill'])
        vecs = []
        for ni in NodeInfo.objects.all():
            if ni.h is not None:
                vecs.append(ni.h + ni.c)

        # keys = list(vector_to_sentence_dict.keys())
        # nodelist = [_name[:-1] for _name in r.keys('Node_*:h')]
        # hlist = [n + b'h' for n in nodelist]
        # clist = [n + b'c' for n in nodelist]
        # vecs = []
        # for h, c in zip(r.mget(hlist), r.mget(clist)):
        #     vecs.append(dill.loads(h).tolist() + dill.loads(c).tolist())
        # for

#         print(type(keys))
        vecs = np.array(vecs)
        print("Training kdtree...")
        kdtree = KDTree(vecs)
        print("Done. Saving...")
        with open(kdtree_path,'wb') as outfile:
            dill.dump(kdtree, outfile)
        print("Done.")
