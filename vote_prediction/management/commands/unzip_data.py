from django.core.management.base import BaseCommand, CommandError
from vote_prediction.utils import unzip_zipfiles

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        unzip_zipfiles()
