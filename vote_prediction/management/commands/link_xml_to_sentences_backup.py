# fpath = '/home/belarm/source/nns/vote_prediction/new/billstatus/BILLSTATUS-113-hres.zip'
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.conf import settings
from vote_prediction.models import hashed_sentences_from_node, xml_from_node, sentence_hashes_from_element
# import hashlib
from lxml import etree
# import io
from py2neo import Node, Relationship
from nltk import tokenize
import numpy as np
import dill
import os
from nltk.tokenize.treebank import TreebankWordTokenizer
from collections import namedtuple

RelationshipData = namedtuple('RelationshipData', 'srcid dstid type attr')


def save_etree(parentid, element, o=0):
    node = settings.GRAPH.run("CREATE (n:XMLElement) RETURN n").data()[0]['n']
    if element.text:
        # node['text'] = element.text
        sentorder = 0
        for senthash in sentence_hashes_from_element(element):
            yield RelationshipData(node.identity, hash_to_nodeid[senthash], 'TEXT', {'ordder':sentorder})
            sentorder += 1

    yield RelationshipData(parentid, node.identity, 'CHILD', {'order':o})
    o = 0
    for child in element.iterchildren():
        yield from save_etree(node.identity, child, o)
        o += 1

d = {}
word_tokenizer = TreebankWordTokenizer()
find_xml_query = """MATCH (n:File {mimetype:'application/xml'}) RETURN n"""
find_senteces_query = """MATCH (n:SENTENCE) RETURN n limit 1000"""

# create_relationship_stmt = """UNWIND $relationships as rel
#                            MATCH (s) WHERE ID(s) = rel.srcid WITH s, rel
#                            MATCH (d:SENTENCE {hash: rel.dsthash}) WITH s, d, rel
#                            CREATE (s)-[r:CONTAINS {order: rel.order}]->(d)
#                            RETURN r"""
create_relationship_stmt = """UNWIND $relationships as rel
                           MATCH (s) WHERE ID(s) = rel.srcid WITH s, rel
                           MATCH (d) WHERE ID(d) = rel.dstid WITH s, d, rel
                           CREATE (s)-[r:rel.type]->(d) with r
                           SET r = rel.attr
                           RETURN r"""


# TODO: Need to switch back to tree-ifying the XML and linking the leaves to sentences


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        dict_path = os.path.sep.join([settings.FILES_DIR,'lookup_tables','hash_to_sentence_dict.dill'])

        # Sure, let's keep 1.5 million nodes in RAM...
        c = 0
        for _node in settings.GRAPH.run(find_xml_query):
            node = _node['n']
            c += 1
            try:
                xmltree = xml_from_node(node)
                rootnode = settings.GRAPH.run("CREATE (n:XMLRoot) return n").data()[0]['n']
                relationships = [
                    # Relationship(node, 'CONTAINS', rootnode)
                    RelationshipData(node.identity, rootnode.identity, 'CONTAINS', {})
                ]
                for relationship in save_etree(rootnode, xmltree):
                    relationships.append(relationship)
                for e, (shash, s) in enumerate(hashed_sentences_from_node(node)):
                    # print(e, shash, s)
                    relationships.append({
                        'srcid':node.identity,
                        'dsthash':shash,
                        'order':e
                    })

                res = settings.GRAPH.run(create_relationship_stmt,relationships=[
                    rel._asdict() for rel in relationships
                ]).data()
                # print(res)
                print(f"Created {len(res)} relationships to {node['name']}")



                # #                 tree = etree.parse(file_path(node.identity))
# #                 sentences = sentences_from_xml(tree)
# #                 for sent in sentences:
# #                     shash = hash(' '.join(sent))
# #                     if shash not in d:
# #                         d[shash] = sent

#                 #                 tree = etree.parse(file_path(node.identity))
# #                 sentences = tokenize.sent_tokenize(' '.join(tree.xpath('//text()')))
# #                 sentences = sentences_from_node(node)
#                 tree = etree.parse(file_path(node.identity))
#                 sentences = sentences_from_xml(tree)
#                 for e,sent in enumerate(sentences):
# #                     parsed = word_tokenizer.tokenize(sent.lower())
#                     shash = hash(' '.join(sent))
#                     print(' '.join(sent))
#                     print(shash, shash in d)
# #                     relationships.append({
# #                         'srcid':node.identity,
# #                         'dsthash':shash,
# #                         'order':e
# #                     })
# #                     print(relationships[-1])
# #                 res = settings.GRAPH.run(create_relationship_stmt,relationships=relationships).data()
# #                 print(res)
# #                 print(f"Created {len(res)} relationships to {node['name']}")

            except etree.XMLSyntaxError:
                print("Malformed XML: {}".format(node['name']))
            if c % 1000 == 0:
                print(f"Finished with {c} bills")
                # break


# class Command(BaseCommand):
#     def handle(self, *args, **kwargs):
#         dict_path = os.path.sep.join([settings.FILES_DIR,'lookup_tables','hash_to_sentence_dict.dill'])
#         print("Loading hashes...")
#         with open(dict_path,'rb') as infile:
#             d = dill.load(infile)
#         print("Done")
#         c = 0
#         for _node in settings.GRAPH.run("match (n:File {mimetype:'application/xml'}) "
#                                          "return n "
#                                        ):
#             node = _node['n']
#             c += 1
#             relationships = []
#             try:
# #                 tree = etree.parse(file_path(node.identity))
# #                 sentences = tokenize.sent_tokenize(' '.join(tree.xpath('//text()')))
#                 sentences = sentences_from_node(node)
#                 for e,sent in enumerate(sentences):
# #                     parsed = word_tokenizer.tokenize(sent.lower())
#                     shash = hash(' '.join(sent))
#                     relationships.append({
#                         'srcid':node.identity,
#                         'dsthash':shash,
#                         'order':e
#                     })
#                     print(relationships[-1])
#                 res = settings.GRAPH.run(create_relationship_stmt,relationships=relationships).data()
#                 print(res)
#                 print(f"Created {len(res)} relationships to {node['name']}")

#             except etree.XMLSyntaxError:
#                 print("Malformed XML: {}".format(node['name']))
#             if c % 100 == 0:
#                 print(f"Finished with {c} bills")
#                 break
