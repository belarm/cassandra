from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import os
import dill
import json
from networkx.readwrite import json_graph
create_stmt = "UNWIND $sentences as sent CREATE (d:SENTENCE {hash:sent.hash,h:sent.h,c:sent.c,text:sent.txt}) RETURN d"

batchsize = 10000

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        dict_path = os.path.sep.join([settings.FILES_DIR,'lookup_tables','hash_to_sentence_dict.dill'])
        print("Loading hashes...")
        with open(dict_path,'rb') as infile:
            d = dill.load(infile)
        print("Done")
        sentences = []
        for k in d.keys():
            with open(settings.GRAPHS_DIR + str(k),'r') as infile:
                graph = json_graph.cytoscape_graph(json.load(infile))
            # print(graph.graph.keys())
            sentences.append({
                'hash':k,
                'h': graph.graph['h'],
                'c': graph.graph['c'],
                'sentence': graph.graph['sentence']
            })
            if len(sentences) == batchsize:
                res = settings.GRAPH.run(create_stmt,sentences=sentences)
                print(f"Created {len(res.data())} sentence nodes")
                sentences = []
        if len(sentences):
            res = settings.GRAPH.run(create_stmt,sentences=sentences)
            print(f"Created {len(res.data())} sentence nodes")
