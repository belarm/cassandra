from django.core.management.base import BaseCommand, CommandError
from vote_prediction.models import graph_path, graph_sentences
from django.conf import settings
import os
class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        hashpath = os.path.sep.join([settings.FILES_DIR, 'graphs', 'by_hash', ''])
        if not os.path.exists(hashpath):
            os.mkdir(hashpath)
        for _node in settings.GRAPH.run('match (n:File {mimetype:"application/xml",parsed:True}) return n'):
            node = _node['n']
            sentences = graph_sentences(node)
            i = 0
            for s in sentences:
                sent_hash = hash(s)
                print(f"mv -v {graph_path(node,i)} {hashpath}{sent_hash}")
                i += 1
