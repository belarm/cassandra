# fpath = '/home/belarm/source/nns/vote_prediction/new/billstatus/BILLSTATUS-113-hres.zip'
from django.core.management.base import BaseCommand, CommandError
from vote_prediction.utils import parse_xml_files

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        parse_xml_files()
