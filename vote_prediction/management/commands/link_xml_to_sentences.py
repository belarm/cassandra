# fpath = '/home/belarm/source/nns/vote_prediction/new/billstatus/BILLSTATUS-113-hres.zip'
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.conf import settings
from vote_prediction.models import hashed_sentences_from_node, xml_from_node, sentence_hashes_from_element, word_tokenizer
# import hashlib
from lxml import etree
# import io
from py2neo import Node, Relationship
from nltk import tokenize
import numpy as np
import dill
import os
from nltk.tokenize.treebank import TreebankWordTokenizer
from collections import namedtuple

RelationshipData = namedtuple('RelationshipData', 'srcid dstid type attr')
dict_path = os.path.sep.join([settings.FILES_DIR,'lookup_tables','hash_to_nodeid.dill'])
hash_to_nodeid = dill.load(open(dict_path,'rb'))

def save_etree(parentid, element, o=0):
    node = settings.GRAPH.run("CREATE (n:XMLElement) RETURN n").data()[0]['n']
    # if hasattr(element, 'text') and element.text:
    if element.text:
        # node['text'] = element.text
        sentorder = 0
        for senthash in sentence_hashes_from_element(element):
            yield RelationshipData(node.identity, hash_to_nodeid[senthash], 'TEXT', {'order':sentorder})
            sentorder += 1

    yield RelationshipData(parentid, node.identity, 'CHILD', {'order':o})
    o = 0
    for child in element.iterchildren():
        yield from save_etree(node.identity, child, o)
        o += 1

d = {}
# word_tokenizer = TreebankWordTokenizer()
find_xml_query = """MATCH (n:File {mimetype:'application/xml'}) WHERE NOT (n)-[:CONTAINS]->() RETURN n"""
find_senteces_query = """MATCH (n:SENTENCE) RETURN n limit 1000"""


# Need to use python formatting here because neo4j doesn't support setting
# relationship attributes at creation time.
create_relationship_stmt = """UNWIND $relationships as rel
                           MATCH (s) WHERE ID(s) = rel.srcid WITH s, rel
                           MATCH (d) WHERE ID(d) = rel.dstid WITH s, d, rel
                           CREATE (s)-[r:{label}]->(d) with r, rel
                           SET r = rel.attr
                           RETURN r"""

# DONE: Need to switch back to tree-ifying the XML and linking the leaves to sentences

class Command(BaseCommand):
    def handle(self, *args, **kwargs):

        # print(len(hash_to_nodeid))
        c = 0
        for _node in settings.GRAPH.run(find_xml_query):
            node = _node['n']
            c += 1
            try:
                xmltree = xml_from_node(node)
                rootnode = settings.GRAPH.run("CREATE (n:XMLRoot) return n").data()[0]['n']
                relationships = [
                    RelationshipData(node.identity, rootnode.identity, 'CONTAINS', {})
                ]
                for relationship in save_etree(rootnode.identity, xmltree.getroot()):
                    # print(relationship)
                    relationships.append(relationship)
                # for rel in relationships:
                #     print(type(rel))
                sorted_rels = sorted(relationships, key=lambda x: x.type)
                to_commit = []
                rtype = sorted_rels[0].type
                count = 0
                for rel in sorted_rels:
                    if rel.type == rtype:
                        # Same type, continue appending
                        to_commit.append(rel._asdict())
                    else:
                        # We have all the nodes of a given type, now, so commit
                        # print(f"Commiting {rtype} links from {node['name']}")
                        res = settings.GRAPH.run(
                            create_relationship_stmt.format(label=rtype),
                            relationships=to_commit
                        ).data()
                        count += len(res)
                        # update rtype, clear to_commit
                        rtype = rel.type
                        to_commit = [rel._asdict()]
                if len(to_commit) > 0:
                    # print(f"Commiting {rtype} links from {node['name']}")
                    res = settings.GRAPH.run(
                        create_relationship_stmt.format(label=rtype),
                        relationships=to_commit
                    ).data()
                    count += len(res)
                # print(sorted_rels)
                # res = settings.GRAPH.run(create_relationship_stmt,relationships=[
                #     rel._asdict() for rel in relationships
                # ]).data()
                # print(f"Created {count} relationships to {node['name']}")

            except etree.XMLSyntaxError:
                print("Malformed XML: {}".format(node['name']))
            if c % 1000 == 0:
                print(f"Finished with {c} bills")
