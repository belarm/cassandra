from django.core.management.base import BaseCommand, CommandError
from vote_prediction.utils import upload_directory

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        upload_directory()
