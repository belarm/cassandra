# fpath = '/home/belarm/source/nns/vote_prediction/new/billstatus/BILLSTATUS-113-hres.zip'
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.conf import settings
from vote_prediction.models import hashed_sentences_from_node, xml_from_node
# import hashlib
from lxml import etree
# import io
from py2neo import Node, Relationship
from nltk import tokenize
import numpy as np
import dill
import os


find_senteces_query = """MATCH (n:SENTENCE) RETURN n"""


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        d = {}
        c = 0
        dict_path = os.path.sep.join([settings.FILES_DIR,'lookup_tables'])
        if not os.path.exists(dict_path):
            os.mkdir(dict_path)
        dict_path = os.path.sep.join([dict_path,'hash_to_nodeid.dill'])
        # hash_to_nodeid = {}
        print("Mapping hashes to nodes:")
        hash_to_nodeid = {}
        c = 0
        for _sentnode in settings.GRAPH.run(find_senteces_query):
            sentnode = _sentnode['n']
            hash_to_nodeid[sentnode['hash']] = sentnode.identity
            if c % 10000 == 0:
                print(f"Loaded {c} sentences...")
            c += 1
        # print("Done")
#         for _node in settings.GRAPH.run("match (n:File {mimetype:'application/xml'}) "
#                                          "where not (n)--(:XMLElement) return n;"):
        print("All done, saving dictionary:")
        with open(dict_path,'wb') as outfile:
            dill.dump(hash_to_nodeid, outfile)
        print("Fin.")
