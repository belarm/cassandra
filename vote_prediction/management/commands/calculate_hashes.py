from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from vote_prediction.utils import hash_combine

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        with settings.GRAPH.session() as session:
            # Not ready for this yet
            for n in session.run():
                pass
