from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from vote_prediction.models import sentence_graphs
import networkx as nx
import dill
import os
# ' '.join([g.node[a]['label'] for a in nx.dfs_preorder_nodes(g) if g.out_degree(a) == 0])


class Command(BaseCommand):
    def handle(self, *args, **options):
        dict_path = os.path.sep.join([settings.FILES_DIR,'lookup_tables'])
        if not os.path.exists(dict_path):
            os.mkdir(dict_path)
        dict_path = os.path.sep.join([dict_path,'vector_to_sentence_dict.dill'])
        vector_to_sentence_dict = {}
        for _node in settings.GRAPH.run("match (n:File {mimetype:'application/xml',parsed:true}) "
                                "where exists(n.sentences) "
                                         "return n;"):
            print(_node['n']['name'])
            for e, g in enumerate(sentence_graphs(_node['n'])):
                # Convert list to tuple and use as index
                vector_to_sentence_dict[
                    tuple(g.graph['vectors'][0][0])
                ] = ' '.join(
                    [g.node[a]['label'] for a in nx.dfs_preorder_nodes(g) if g.out_degree(a) == 0]
                )
        with open(dict_path,'wb') as outfile:
            dill.dump(vector_to_sentence_dict, outfile)