from django.shortcuts import render
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from django.views.generic.detail import DetailView
from django.shortcuts import redirect
from django.http import JsonResponse, HttpResponse
from .models import store_uploaded_file, graph_path, sentences_from_node, xml_from_node, hashed_sentences_from_node, hashed_sentences_from_uid, sentences_from_xml
from .forms import UploadForm
import pickle
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.figure import Figure
import matplotlib
import io
import os
from django.conf import settings
import mimetypes
from .custom_paginator import CustomPaginator, InvalidPage
import json

#KEEP
class NodeListView(ListView):
    template_name = 'vote_prediction/node_list.html'
    paginate_by=50
    context_object_name='File'
    object_list = None
    filters={
        'mimetype':'application/xml',
        # 'parsed':True
    }
    def get_queryset(self, **kwargs):
        res = settings.GRAPH.run("match (n:File {mimetype:'application/xml'}) "
                                         "return n "
                                         f"limit {self.paginate_by}"
                                )
        print(kwargs)
        print("Getting queryset")
        print(len(res))
        return list(res)
    def get(self, request, *args, **kwargs):
        allow_empty = True
        context = self.get_context_data()
        return self.render_to_response(context)

    def get_context_data(self, *, object_list=None, **kwargs):
        """Get the context for this view."""
        page_size = self.paginate_by
        context_object_name = self.context_object_name
        if page_size:
            paginator, page, queryset, is_paginated = self.paginate_queryset(page_size)
            context = {
                'paginator': paginator,
                'page_obj': page,
                'is_paginated': is_paginated,
                'object_list': queryset
            }
        else:
            context = {
                'paginator': None,
                'page_obj': None,
                'is_paginated': False,
                'object_list': queryset
            }
        if context_object_name is not None:
            context[context_object_name] = queryset
        context.update(kwargs)
        return context

    def paginate_queryset(self, page_size):
        """Paginate the queryset, if needed."""
        paginator = CustomPaginator(
            self.context_object_name, page_size, self.filters, orphans=self.get_paginate_orphans(),
            allow_empty_first_page=True)
        page_kwarg = self.page_kwarg
        page = self.kwargs.get(page_kwarg) or self.request.GET.get(page_kwarg) or 1
        try:
            page_number = int(page)
        except ValueError:
            if page == 'last':
                page_number = paginator.num_pages
            else:
                raise Http404(_("Page is not 'last', nor can it be converted to an int."))
        try:
            page = paginator.page(page_number)
            return (paginator, page, page.object_list, page.has_other_pages())
        except InvalidPage as e:
            raise Http404(_('Invalid page (%(page_number)s): %(message)s') % {
                'page_number': page_number,
                'message': str(e)
            })
#KEEP
class NodeAsGraph(DetailView):
    template_name = 'vote_prediction/node_as_graph_detail.html'
    def get_context_data(self, **kwargs):
        node = self.get_object()
        return {
                    'node': node,
                    'sentence': self.kwargs['sentence'],
                    # 'available_sentences': list(range(node['sentences']))
                    'available_sentences': list(range(len(sentences_from_node(node))))
               }

    def get_object(self):
        res = settings.GRAPH.run(f"match (n) where ID(n) = {self.kwargs['pk']} return n").data()[0]['n']
        res['identity'] = res.identity
        return res

def delete_node(request, pk):
#     pk = request.GET['pk']
    Node.objects.get(pk=pk).delete()
    # Return from whence you came...
    return redirect(request.META['HTTP_REFERER'])

def _nodelabel(node):
    return '{}\n{}'.format(node.pk, node['name'])

def nodegraph(request, pk, sentence):
    sentence_pairs = hashed_sentences_from_uid(pk)
    # node = settings.GRAPH.run(f"match (n) where ID(n) = {pk} return n").data()[0]['n']
    # print(graph_path(node, sentence))
    # with open(graph_path(node, sentence)) as infile:
    with open(os.path.sep.join([settings.FILES_DIR, 'graphs', 'by_hash', str(sentence_pairs[sentence][0])]),'r') as infile:
        return HttpResponse(infile,content_type = 'application/json; charset=utf8')
class UploadFileView(FormView):
    form_class = UploadForm
    template_name = 'upload.html'
    success_url = '/'


    def post(self, request, *args, **kwargs):
        form = self.get_form(self.get_form_class())
        files = request.FILES.getlist('files')
        if form.is_valid():
            for f in files:
                print("Creating {} Node \"{}\"".format(f.content_type, f.name))
                mimetype, encoding = mimetypes.guess_type(f.name)
                res = settings.GRAPH.run("CREATE (n:File {{name: '{}', mimetype: '{}'}}) RETURN n".format(
                    f.name,
                    mimetype)).data()
                print(res[0]['n'].identity)
                store_uploaded_file(res[0]['n'].identity, f)

            return self.form_valid(form)
        else:
            return self.form_invalid(form)
