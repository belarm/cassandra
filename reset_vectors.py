#!/usr/bin/env python3
from cassandra import wsgi
from django.conf import settings
with settings.GRAPH.session() as session:
    session.run("match(n) where exists(n.h) remove n.h, n.c return id(n)")
