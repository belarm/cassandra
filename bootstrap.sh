#!/bin/bash
git submodule init
git submodule update --remote --merge
cp env-testing .env
docker-compose build
docker-compose up -d
# echo "Sleeping 30s for things to settle down..."
# sleep 30
docker-compose run cassandra ./init_data.sh
# docker-compose exec cassandra ./fill_graphs_batch.py
