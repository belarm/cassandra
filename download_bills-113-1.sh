#!/bin/bash

tmpfile=`mktemp`
#zOMG hax
grep ^BILLS_DIR cassandra/settings.py > $tmpfile
. $tmpfile
echo Downloading bills to $BILLS_DIR ...
cd $BILLS_DIR
dirs="sres sjres sconres s hres hr hjres hconres"
congresses="113"
sessions="1"
for congress in $congresses
do
    for session in $sessions
    do
        for dir in $dirs
        do
        #https://www.govinfo.gov/bulkdata/BILLSTATUS/115/sres/BILLSTATUS-115-sres.zip
            url="https://www.govinfo.gov/bulkdata/BILLS/${congress}/${session}/${dir}/BILLS-${congress}-${session}-${dir}.zip"
            echo "Downloading $url"
            wget -q -c $url
        done
    done
done
rm $tmpfile
