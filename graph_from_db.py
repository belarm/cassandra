#!/usr/bin/env python3
from cassandra import wsgi
from django.conf import settings
import networkx as nx
from networkx.drawing.nx_pydot import graphviz_layout
from vote_prediction.models import graph_from_db
from collections import defaultdict, namedtuple
from snli.model import SNLIModel
from utils.vocab import Vocab
import torch
import numpy as np
ChildrenAndVectors = namedtuple("ChildrenAndVectors", "children hs cs")

def roots(graph):
    for n in graph:
        if graph.in_degree(n) == 0:
            yield n

torch.set_grad_enabled(False)
vocab = Vocab.from_file('pretrained/snli_vocab.txt', add_pad=True, add_unk=True)
model = SNLIModel(num_classes=3, num_words=len(vocab), word_dim=300, hidden_dim=300,
                  clf_hidden_dim=1024, clf_num_layers=1, bidirectional=False, dropout_prob=0,
                  use_leaf_rnn=True, intra_attention=False, use_batchnorm=True)
model.load_state_dict(torch.load('pretrained/snli_300d.pkl', map_location='cpu'))
null_vector = model.word_embedding(torch.as_tensor([[vocab.pad_id]]))
null_state = model.encoder.forward(null_vector,torch.as_tensor([1]))
class GraphEmbedder(object):
    '''Calculates embeddings for all nodes in a graph'''
    def __init__(self, graph, model, null_state=null_state, batchsize=100):
        self.frontier = set()
        self.open = set()
        self.close = set()
        self.graph = graph
        self.batchsize = batchsize
        self.model = model
        self.children_to_node = defaultdict(set)
        self.null_state = null_state
        for n in self.graph:
            self.children_to_node[self.childrenof(n)].add(n)
            if 'h' in self.graph.node[n]:
                self.close.add(n)
            else:
                self.open.add(n)

    def isready(self, node):
        for e in self.graph.adj[node].keys():
            if e not in self.close:
                return False
        return True

    def childrenof(self, n):
        return tuple(child for child in self.graph.adj[n].keys())

    def haschildren(self, node):
        if len(self.childrenof(node)) > 0:
            return True
        else:
            return False

    def nodevectors(self, nodes):
        ret = []
        for node in nodes:
            ret.append((
            self.graph.node[node]['h'],
            self.graph.node[node]['c']
        ))
        return ret

    def node_states(self, nodes):
        '''Returns the h and c attributes of the passed nodes as two lists'''
        if len(nodes) == 0:
            return self.null_state[0], self.null_state[1]
        hs, cs = [], []
        for node in nodes:
            # print(self.graph.node[node].keys())
            if 'h' not in self.graph.node[node]:
                print()
            hs.append(torch.as_tensor(self.graph.node[node]['h']))
            cs.append(torch.as_tensor(self.graph.node[node]['c']))
        return torch.stack(hs), torch.stack(cs)

    def populate_frontier(self):
        self.frontier = set()
        for node in self.graph:
            if node in self.open and self.isready(node):
                self.frontier.add(node)
        bl = len(self.open)
        self.open.difference_update(self.frontier)
        al = len(self.open)
        print(f"Removed {bl - al} nodes from open")
        self.close.update(self.frontier)

    def print_sets(self):
        lastline = ""
        todo = [
            (self.open, 'open  set'),
            (self.close, 'close set'),
            (self.frontier, 'frontier '),
        ]
        for set, name in todo:
            lines = defaultdict(lambda : 0)
            print(f"------------------{name}----------------------")
            for node in set:
                line = f"{'h' in self.graph.node[node]:5} {list(self.graph.node[node].keys())} {self.graph.node[node]['_labels']}"
                lines[line] += 1
            for k, v in lines.items():
                print(f"{v:10} {k}")

    def embed_frontier(self):
        if len(self.frontier) == 0:
            return
        input_h = []
        input_c = []
        idx_to_node = []
        curlist = sorted(self.frontier, key=lambda x: len(self.childrenof(x)))
        for node in curlist:
            children = self.childrenof(node)
            child_hs, child_cs = self.node_states(children)
            input_h.append(child_hs)
            input_c.append(child_cs)
            idx_to_node.append(node)
        c = 0
        lastl = len(input_h[0])
        submitted_i = 0
        hs, cs = [], []
        for i in range(len(curlist)):
            diff = i - submitted_i
            newl = input_h[i].size()[0]
            if diff == self.batchsize or newl != lastl:
                hs, cs = self.model.encoder.forward_with_branches(
                    torch.stack(input_h[submitted_i:i]),
                    torch.stack(input_c[submitted_i:i])
                )
                for e, (_h, _c) in enumerate(zip(hs,cs)):
                    self.graph.node[idx_to_node[e + submitted_i]]['h'] = _h.tolist()
                    self.graph.node[idx_to_node[e + submitted_i]]['c'] = _c.tolist()
                submitted_i = i
                lastl = newl
        hs, cs = self.model.encoder.forward_with_branches(
            torch.stack(input_h[submitted_i:]),
            torch.stack(input_c[submitted_i:])
        )
        for e, (_h, _c) in enumerate(zip(hs,cs)):
            self.graph.node[idx_to_node[e + submitted_i]]['h'] = _h.tolist()
            self.graph.node[idx_to_node[e + submitted_i]]['c'] = _c.tolist()

        print("After embedding:")
        self.print_sets()

g = graph_from_db()
print("Got graph!")
geb = GraphEmbedder(g.copy(), model)
while len(geb.open):
    print("Populating frontier...")
    geb.populate_frontier()
    print("Embedding nodes from frontier...")
    geb.embed_frontier()
