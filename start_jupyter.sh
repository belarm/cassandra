#!/bin/bash
docker run --name cassandra_jupyter -d --rm -p 8888:8888 -e JUPYTER_LAB_ENABLE=yes -v "$PWD":/home/jovyan/work jupyter/datascience-notebook
