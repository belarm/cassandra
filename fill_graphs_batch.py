#!/usr/bin/env python3
# from cassandra import wsgi
# from django.conf import settings



################################################################################
#
# THIS IS OUR LEAF TRANSFORM
#
################################################################################






import os
import json
from networkx.readwrite import json_graph
import torch
from snli.model import SNLIModel
from utils.vocab import Vocab
import networkx as nx
from networkx.drawing.nx_pydot import graphviz_layout
from lxml import etree
from nltk import tokenize
import dill
import numpy as np
import datetime
from cassandra import wsgi
from django.conf import settings
import matplotlib.pyplot as plt
torch.set_grad_enabled(False)
vocab = Vocab.from_file('pretrained/snli_vocab.txt', add_pad=True, add_unk=True)
model = SNLIModel(num_classes=3, num_words=len(vocab), word_dim=300, hidden_dim=300,
                  clf_hidden_dim=1024, clf_num_layers=1, bidirectional=False, dropout_prob=0,
                  use_leaf_rnn=True, intra_attention=False, use_batchnorm=True)
model.load_state_dict(torch.load('pretrained/snli_300d.pkl', map_location='cpu'))
model.eval()
def prepare_input(sent_words):
    word_ids = torch.LongTensor([[vocab.word_to_id(w) for w in sent_words]])
    length = torch.LongTensor([len(sent_words)])
    word_ids_emb = model.word_embedding(word_ids)
    return word_ids_emb, length

# model.encoder.forward(*prepare_input(['a', 'b', 'c']), return_select_masks=True)[2]
from nltk.tokenize import word_tokenize
def to_tree_str(sentence):
    # words = word_tokenize(sentence.lower())
    words = sentence
    enc_input, enc_length = prepare_input(words)
    selections = model.encoder.forward(enc_input, enc_length, return_select_masks=True)[2]
    selections = [s[0].max(0)[1] for s in selections] + [0]
    tokens = words.copy()
    for i in selections:
        composed = f'( {tokens[i]} {tokens[i+1]} )'
        del tokens[i:i+2]
        tokens.insert(i, composed)
    assert len(tokens) == 1
    return tokens[0]

def make_graph(output, sentence):
    g = nx.DiGraph()
    selections = output[2]
    # print(f"Selections: length={length}")
    # print(selections)
    vectors = [output[0].tolist(), output[1].tolist()]
    # WRONG
    # selections = [s[0].max(0)[1] for s in selections] + [0]
    selections = [s.argmax() for s in selections] + [0]
    length = len(selections) + 1

    # print(selections)
    tokens = list(range(length))
    for w in sentence:
        g.add_node(len(g), label=str(w))

    # Sure looks like this is your problem
    for i in selections:
        newidx = len(g)
        # print(f"Linking {newidx}->{tokens[i]} and {tokens[i+1]}")
        g.add_node(newidx, label=newidx)
        g.add_edge(newidx, tokens[i], idx=0)
        g.add_edge(newidx, tokens[i + 1], idx=1)
        del tokens[i:i+2]
        tokens.insert(i, newidx)
    assert len(tokens) == 1
    g.graph['root'] = len(g)-1
    g.graph['h'] = vectors[0] # Hidden state
    g.graph['c'] = vectors[1] # Cell state
    g.graph['sentence'] = ' '.join(sentence)
    return g
c = 0
sentences = []
print("Loading hashes...")



# THIS IS WHAT WE NEED TO CHANGE
d = {}
for _n in settings.GRAPH.run("match (n:Sentence) return n"):
    n = _n['n']
    d[n['hash']] = n['text']
# with open(settings.FILES_DIR + '/lookup_tables/hash_to_sentence_dict.dill','rb') as infile:
#     d = dill.load(infile)
print("Done")


batchsize = 100
sorted_by_size = sorted(list(d.keys()), key=lambda x: len(d[x]))
# sorted_by_size = list(d.keys())

def do_batch(batch, maxlen):
    print(f"{datetime.datetime.now()}: Running batch of size {len(batch)}x{maxlen}")
    vectors = []
    lens = []
    for entry in batch:
        # print([vocab.word_to_id(w) for w in entry] + [vocab.pad_id] * (maxlen - len(entry)))
        shit = model.word_embedding(
                torch.LongTensor([vocab.word_to_id(w) for w in entry] + [vocab.pad_id] * (maxlen - len(entry)))
            ).tolist()
        # print(shit)
        vectors.append(shit)
        lens.append(len(entry))
    return model.encoder.forward(torch.Tensor(vectors),torch.LongTensor(lens), return_select_masks=True)

batch = []
ids = []
mlen = 0
batchnum = 0
while(True):
#     try:
    try:
        k = sorted_by_size.pop(0)
    except IndexError as e:
        break
    graphpath = settings.GRAPHS_DIR + str(k)
    if os.path.exists(graphpath):
        continue
    else:
        batch.append(d[k])
        ids.append(str(k))
        mlen = max(mlen,len(d[k]))
        if len(batch) == batchsize:
            v0, v1, selections = do_batch(batch, mlen)
            # print(selections)
            for i in range(len(batch)):
                g = make_graph((
                            v0[i],
                            v1[i],
                            [elem[i] for elem in selections]
                        ),
                        batch[i]
                    )
                filepath = settings.GRAPHS_DIR + ids[i]
                with open(filepath,'w') as outfile:
                    json.dump(json_graph.cytoscape_data(g), outfile)
            mlen = 0
            batch = []
            ids = []
            batchnum += 1
if len(batch) > 0:
    print("running final batch")
    v0, v1, selections = do_batch(batch, mlen)
    # print(selections)
    for i in range(len(batch)):
        g = make_graph((
                    v0[i],
                    v1[i],
                    [elem[i] for elem in selections]
                ),
                batch[i]
            )
        filepath = settings.GRAPHS_DIR + ids[i]
        with open(filepath,'w') as outfile:
            json.dump(json_graph.cytoscape_data(g), outfile)
