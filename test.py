#!/usr/bin/env python3

from vote_prediction import utils
print("Getting nodes...")

nodes = utils.settings.GRAPH.run('match(n) return n')
print("Getting edges...")
edges = utils.settings.GRAPH.run('match ()-[r]-() return r')
