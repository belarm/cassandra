#!/bin/bash
# sleep 20
# Run manage.py once to initalize directories, etc
wait-for-it -t 60 db:5432 -- wait-for-it -t 60 graphdb:7687 -- ./manage.py migrate
# wait-for-it -t 60 graphdb:7687 -- ./manage.py

echo ./download_bills-subset.sh
./download_bills-subset.sh

# echo ./download_bills-113-1.sh
# ./download_bills-113-1.sh

echo ./manage.py upload_files
./manage.py upload_files


echo ./manage.py unzip_data
./manage.py unzip_data

echo ./manage.py parse_xml
./manage.py parse_xml
# echo ./fill_graphs_batch.py
# ./fill_graphs_batch.py
# # echo ./manage.py make_sentence_nodes
# # ./manage.py make_sentence_nodes
# # echo ./manage.py make_senthash_dict
# # ./manage.py make_senthash_dict
# # echo ./manage.py link_xml_to_sentences
# # ./manage.py link_xml_to_sentences
# echo ./graph_from_db.py
# ./graph_from_db.py


# echo OK, running embeddings...
python -m vote_prediction.graph_embedder
#
# echo Building KDTree....
# ./manage.py build_kdtree
