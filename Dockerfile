FROM python:3.6-stretch
# Current blocker: nltk
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
RUN mkdir /code/networkx
RUN mkdir /code/networkx/requirements
WORKDIR /code
RUN apt-get update && apt-get install -y \
    postgresql-client \
    graphviz \
    wait-for-it \
 && rm -rf /var/lib/apt/lists/*
ADD requirements.txt /code/
RUN pip install -r requirements.txt
RUN echo "import nltk; nltk.download('punkt')" | python3
# ADD . /code/
