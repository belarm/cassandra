"""
Django settings for cassandra project.

Generated by 'django-admin startproject' using Django 2.1.1.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""

import os
import urllib.request
# print("ENTERED SETTINGS")

# from py2neo import Graph
from neo4j.v1 import GraphDatabase
import redis
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ch@norr#xf3h_x+n-!zn5pi4op^-d$n#vg$d70tq&*=e$ne+4p'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'vote_prediction'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'cassandra.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'cassandra.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ['CASSANDRA_DB'],
#         'NAME': 'cassandra',
        'USER': os.environ['CASSANDRA_DB_USER'],
#         'USER': 'cassandra_user',
        'PASSWORD': os.environ['CASSANDRA_DB_PASSWORD'],
#         'PASSWORD': 'passwordssuck',
#         'HOST': 'localhost',
#         'HOST': '192.168.1.13',
        'HOST': os.environ['CASSANDRA_DB_HOST'],
#         'HOST': 'db',
        'PORT': '5432',
    }
}
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': 'mydatabase',
#     }
# }
try:
    ALLOWED_HOSTS = [
        'localhost',
        '127.0.0.1',
        '192.168.1.13',
        '0.0.0.0',
        '*'
#         urllib.request.urlopen("http://169.254.169.254/latest/meta-data/public-ipv4").read().decode(),
#         urllib.request.urlopen("http://169.254.169.254/latest/meta-data/public-hostname").read().decode()
    ]
except:
    ALLOWED_HOSTS = [
        'localhost',
        '127.0.0.1',
        '192.168.1.13',
        '0.0.0.0',
        '*'
    ]


# print("Allowed hosts: {}".format(ALLOWED_HOSTS))

# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

# STATIC_URL = 'http://192.168.1.2:90/'
STATIC_URL =  os.environ['DJANGO_STATIC_URL']

# BILLS_DIR='/var/lib/cassandra/bills'
# BILLS_DIR='/home/belarm/source/congress/data/112'
BILLS_DIR='/var/lib/cassandra/bills/'
MODELS_DIR='/var/lib/cassandra/models/'
GRAPHS_DIR='/var/lib/cassandra/graphs/'
FILES_DIR='/var/lib/cassandra/files/'
LUTABLES_DIR='/var/lib/cassandra/files/lookup_tables'
# VOTES_DIR='/home/belarm/source/congress/data'
for d in [BILLS_DIR, MODELS_DIR, GRAPHS_DIR, FILES_DIR, LUTABLES_DIR]:
    if not os.path.exists(d):
        os.mkdir(d)
VECTOR_SIZE=200
# GRAPH = Graph(host='graphdb',user='neo4j',password='password')
# GRAPH = Graph(host='graphdb')
GRAPH = GraphDatabase.driver('bolt://graphdb')
REDIS = redis.Redis('redis')
